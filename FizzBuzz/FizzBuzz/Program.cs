﻿using System.Collections.Generic;
using FizzBuzz.Conditions;
using FizzBuzz.Printers;
using FizzBuzz.RuleCollections;
using FizzBuzz.Rules;
using FizzBuzz.Shared;
using FizzBuzz.Strategy;
using FizzBuzz.Tags;

namespace FizzBuzz
{
    static class Program
    {
        static void Main()
        {
            var numTags = new TagNameRuleCollection(new List<IRule>()
            {
                new TagNameRule(new Tag("FizzBuzz"), new AndStrategy(new List<ITruthy>
                {
                    new DivCondition(3), new DivCondition(5)
                })),
                new TagNameRule(new Tag("Fizz"), new AndStrategy(new List<ITruthy>
                {
                    new DivCondition(3)
                })),
                new TagNameRule(new Tag("Buzz"), new AndStrategy(new List<ITruthy>
                {
                    new DivCondition(5)
                }))
            });

            for (int i = 0; i < 100; i++)
            {
                new ConsolePrinter(numTags.Find(i, new Tag(i.ToString()))).Print();
            }
        }
    }
}