﻿namespace FizzBuzz.Tags
{
    public class Tag : ITag
    {
        public string Value { get; }
        
        public Tag(string value)
        {
            Value = value;
        }
    }
}