﻿using FizzBuzz.Strategy;
using FizzBuzz.Tags;

namespace FizzBuzz.Rules
{
    public class TagNameRule : IRule
    {
        public ITag Tag { get; }
        public IStrategy Strategy { get; }

        public TagNameRule(ITag tag, IStrategy strategy)
        {
            Tag = tag;
            Strategy = strategy;
        }

        public bool IsSuccess(int num)
        {
            return Strategy.IsTruthy(num);
        }
    }
}