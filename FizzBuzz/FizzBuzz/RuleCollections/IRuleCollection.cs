﻿using System.Collections.Generic;
using FizzBuzz.Rules;
using FizzBuzz.Tags;

namespace FizzBuzz.RuleCollections
{
    public interface IRuleCollection
    {
        List<IRule> Tags { get; }

        ITag Find(int num, ITag defaultValue);
    }
}