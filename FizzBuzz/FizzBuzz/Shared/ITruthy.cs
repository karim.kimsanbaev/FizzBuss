﻿namespace FizzBuzz.Shared
{
    public interface ITruthy
    {
        bool IsTruthy(int num);
    }
}