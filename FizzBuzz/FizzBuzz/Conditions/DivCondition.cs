﻿namespace FizzBuzz.Conditions
{
    public class DivCondition : ICondition
    {
        private readonly int _divider;

        public DivCondition(int divider)
        {
            _divider = divider;
        }

        public bool IsTruthy(int num)
        {
            return num % _divider == 0;
        }
    }
}