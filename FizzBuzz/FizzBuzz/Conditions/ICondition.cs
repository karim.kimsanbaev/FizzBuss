﻿using FizzBuzz.Shared;

namespace FizzBuzz.Conditions
{
    public interface ICondition : ITruthy
    {
    }
}